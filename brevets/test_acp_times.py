"""
Nose tests for acp_times.py
"""
import acp_times

def test_zero():
    """
    Ensure the opening and closing time for the start
    """
    assert acp_times.open_time(0, 200, '2020-01-01T00:00:00+00:00') == '2020-01-01T01:00:00+00:00'
